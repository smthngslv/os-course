#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define SUCCESS 			0
#define INVALID_INDEX_ERROR 1
#define NULL_POINTER_ERROR  2

// Instance of node.
struct Node {
	struct Node* child;
	
	int32_t  value;
};

// Now we can use Node_t as a type.
typedef struct Node Node_t;

// Instance of linked list.
struct LinkedList {
	Node_t* head;
	
	uint32_t size;
};

// Now we can use LinkedList_t as a type.
typedef struct LinkedList LinkedList_t;


// Prints the `list`.
void print_list(LinkedList_t* const list) {	
	printf("LinkedList(size = %d) { ", list->size);
	
	Node_t* current_node = list->head;
	
	// If current_node is NULL then we reach the end of the list.
	while(current_node) {
		printf("%d ", current_node->value);
		
		// Move forward.
		current_node = current_node->child;
	}
	
	printf("} \n");
}

// Inserts a new node with `value` in position `index` of the given `list`. 
// Returns a error code of 0 if success.
int32_t insert_node(LinkedList_t* const list, const uint32_t index, const int32_t value) {
	if (list->size < index) { 
		return INVALID_INDEX_ERROR;
	}
	
	Node_t* new_node = (Node_t*)malloc(sizeof(Node_t));
	
	new_node->child = NULL;
	new_node->value = value;
	
	// Insert at the beginning.
	if (index == 0u) {
		new_node->child = list->head;
		
		list->head = new_node;
		
		++list->size;
		
		return SUCCESS;
	}
	
	Node_t* current_node = list->head;
	
	for (uint32_t i = 0u; i < index - 1u; ++i) {
		// Move forward.
		current_node = current_node->child;
		
		if (!current_node) {
			free(new_node);
			
			return NULL_POINTER_ERROR;
		}
	}
	
	// Rewrite pointer.
	new_node->child = current_node->child;
	current_node ->child = new_node;
	
	++list->size;
	
	return SUCCESS;
}

// Deletes node in position `index` of the given `list`. 
// Returns a error code of 0 if success.
int32_t delete_node(LinkedList_t* const list, const uint32_t index) {
	if (list->size < index) { 
		return INVALID_INDEX_ERROR;
	}
	
	// Delete at the beginning.
	if (index == 0u) {
		Node_t* old_node = list->head;
		
		list->head = old_node->child;
		
		free(old_node);
		
		--list->size;
		
		return SUCCESS;
	}
	
	Node_t* parent  = NULL;
	Node_t* current_node = list->head;
	
	for (uint32_t i = 0u; i < index; ++i) {
		// Save previous node.
		parent = current_node;
		
		// Move forward.
		current_node = current_node->child;
		
		if (!current_node) {
			return NULL_POINTER_ERROR;
		}
	}
	
	// Rewrite pointer.
	parent->child = current_node->child;
	
	free(current_node);
	
	--list->size;
	
	return SUCCESS;
}


// Creates an empty lis.
LinkedList_t* create_list(void) {
	LinkedList_t* list = (LinkedList_t*)malloc(sizeof(LinkedList_t));
	
	list->size = 0u;
	list->head = NULL;

	return list;
}

// Free all memory.
void delete_list(LinkedList_t* const list) {
	for (uint32_t i = 0; i < list->size; ++i) {
		delete_node(list, 0);
	}
		
	free(list);
}


int32_t main(void) {
	LinkedList_t* list = create_list();
	
	print_list(list);
	
	printf("%d -> ", insert_node(list, 0, 22)); print_list(list);
	printf("%d -> ", insert_node(list, 0, 23)); print_list(list);
	printf("%d -> ", insert_node(list, 1, 24)); print_list(list);
	printf("%d -> ", insert_node(list, 3, 25)); print_list(list);
	
	printf("%d -> ", delete_node(list, 0)); print_list(list);
	printf("%d -> ", delete_node(list, 2)); print_list(list);
	printf("%d -> ", delete_node(list, 1)); print_list(list);
	printf("%d -> ", delete_node(list, 0)); print_list(list);
	
	delete_list(list);
	
	return 0;
}