#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#define PATH "./tmp/"

#define MAX_INODES      256
#define MAX_PATH_LENGTH 256

// To store combined path.
char path[MAX_PATH_LENGTH];
    
// To get info about file.
struct stat     info;
// To get entry in directory.
struct dirent* entry; 
    
// Number of already printer inodes.
uint32_t inodes_count = 0;
// Store all printed inodes to do not print it several times.
ino_t printed_inodes[MAX_INODES] = {0};

// Checks if inode is already printed. 
uint32_t is_already_printed(const ino_t inode) {
    for (uint32_t i = 0; i < inodes_count; ++i) {
        if (printed_inodes[i] == inode) {
            return 1;
        }
    }
    
    return 0;
}

// Print inode info.
void print_inode_info(const ino_t inode) {
    if (is_already_printed(inode)) {
        return;
    }
    
    // If the inodes buffer is full.
    if (inodes_count == MAX_INODES) {
        perror("Inodes buffer is full!\n");
        
        // Error.
        exit(1);
    }
    
    // Remember the inode.
    printed_inodes[inodes_count++] = inode;
    
    printf("i-node: %ld\n\tfiles:\n", inode);
    
    DIR* directory_ = opendir(PATH);
    
    // Find all files with the same inode.
    while (entry = readdir(directory_)) {
        // Build a path.
        strcpy(path, PATH);
        strcat(path, entry->d_name);
        
        if (!stat(path, &info) && info.st_ino == inode) {
            printf("\t\t%s\n", path);
        }
    }
    
    closedir(directory_);
    
    printf("\n");
}

int32_t main(void) {
    DIR* directory = opendir(PATH);
    
    while (entry = readdir(directory)) {
        // Build a path
        strcpy(path, PATH);
        strcat(path, entry->d_name);
        
        if (!stat(path, &info) && info.st_nlink >= 2) {
            print_inode_info(info.st_ino);
        }
    }
    
    closedir(directory);
    
    return 0;
}