#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


int32_t main(void) {
    int32_t n;
    
    printf("Enter the N = "); scanf("%d", &n);

    int32_t* array = (int32_t*)malloc(sizeof(int32_t) * n);
    
    if (!array) {
        perror("Cannot allocate memory.\n");
        
        return 1;
    }
    
    for (uint32_t i = 0; i < n; ++i) {
        array[i] = i;
        
        printf("%d ", array[i]);
    }
    
    printf("\n");
    
    free(array);
    
    return 0;
}