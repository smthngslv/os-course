#include <stdio.h>

// First Variant.
int main() {
    char **s;
    char foo[] = "Hello World";
    
    // `s` is uninitialized, so we cannot access a pointer which this pointer points to.
    //*s = foo;
    // We can do follow:
    char* ptr;
    
    // Initialize `s`:
    s = &ptr;
    
    //And only now we can assign an address to the pointer which `s` points to:
    // Now `ptr` is `foo`
    *s = foo;
    
    // `s` is pointer, so we need to get the value(anouther pointer) to print the line.
    //printf("s is %s\n",s);
    printf("s is %s\n",*s);
    
    // It's OK because `s[0]` is the same as `*(s + 0)`, so, same as `*s`.
    s[0] = foo;
    
    // It's OK because `s[0]` is the same as `*(s + 0)`, so, same as `*s`. But it prints all line, because of `%s`.
    // If we need to print first char of the string, we should use `%s` instead of `%c` and `(*s)[0]` instead of `s[0]`
    // printf("s[0] is %s\n",s[0]);
    printf("s[0] is %c\n",(*s)[0]);
    
    return(0);
}


/* Second Variant
int main() {
    // I replace `**s` to `*s`.
    //char **s;
    char *s;
    char foo[] = "Hello World";

    // Since `s` is just pointer to the char:
    //*s = foo;
    s = foo;
    
    // It's OK
    printf("s is %s\n",s);
    
    // We cannot assign char* to the char. But we can get first char by using *foo.
    //s[0] = foo;
    s[0] = *foo;
    
    // We need to replace `%s` by `%c`.
    //printf("s[0] is %s\n",s[0]);
    printf("s[0] is %c\n",s[0]);
    
    return(0);
}
*/