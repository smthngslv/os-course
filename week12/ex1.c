#include <stdio.h>
#include <stdint.h>

/* 
Actually urandom is more secure and never blocks, 
so, it is more convenient.

#define INPUT         "/dev/urandom"
*/

#define INPUT         "/dev/random"
#define OUTPUT        "ex1.txt"
#define CHARS_TO_READ 20


int32_t main(void) {
    FILE* input = fopen(INPUT, "r");
    
    if (!input) {
        printf("Cannot open input.\n");
        
        return 1;
    }
    
    
    for (uint32_t i = 0; i < CHARS_TO_READ; ++i) {
        int32_t c = fgetc(input);
        
        if (c == EOF) {
            printf("Cannot read the input.\n");
            
            break;
        }
        
        printf("%c", c);
    }
    
    printf("\n");
    
    fclose(input);
}