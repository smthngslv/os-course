#include <fcntl.h>
#include <errno.h> 
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUFFER_SIZE 128

int32_t main(int32_t argc, char* argv[]) { 
    // 1 if -a parameter is provided and 0 otherwise.
    int32_t is_append = (argc > 1 && !strcmp(argv[1], "-a")) ? 1 : 0;
    
    // Using buffer is faster then coping byte by byte.
    uint8_t* buffer = (uint8_t*)malloc(sizeof(uint8_t) * BUFFER_SIZE);
    
    // Array of file descriptors. 
    //files[0] is always "/dev/stdin"
    //files[1] is always "/dev/stdout"
    int32_t* files  = (int32_t*)malloc(sizeof(int32_t) * (argc - is_append));
    
    if (!files) {
        printf("Cannot allocate memory for files.\n");
        
        return errno;
    }
    
    if (!buffer) {
        printf("Cannot allocate memory for buffer.\n");
        
        free(files);
        
        return errno;
    }
    
    // Open files.
    for (uint32_t i = 0; i < (argc - is_append + 1); ++i) {      
        switch (i) {
            //files[0] is always "/dev/stdin"
            case 0:
                files[i] = open("/dev/stdin", O_RDONLY);
            
                break;
             
            //files[1] is always "/dev/stdout"
            case 1:
                files[i] = open("/dev/stdout", O_WRONLY);
            
                break;
                
            default:
                // If file does not exist - creates it with 777 permissions.
                files[i] = open(argv[i + is_append - 1], O_WRONLY | O_CREAT | (is_append ? O_APPEND : 0), S_IRWXU | S_IRWXG | S_IRWXO);
        }
        
        // If not opens.
        if (files[i] < 0) {
            printf("Cannot open %d file: '%s'.\n", i - 1, argv[i + is_append - 1]);
            
            for (uint32_t j = 0; j < i; ++j){
                close(files[j]);
            }
        
            free(files);
            free(buffer);
        
            return errno; 
        }
    }
    
    int32_t bytes_read = 0;
    
    // Read until EOF.
    while ((bytes_read = read(files[0], buffer, BUFFER_SIZE)) > 0) {
        // Write to each file except files[0] ("dev/stdin").
        for (uint32_t i = 1; i < (argc - is_append + 1); ++i) {
            int32_t bytes_write = 0;
        
            do {
                int32_t count = write(files[i], buffer + bytes_write, bytes_read - bytes_write);
                
                if (count <= 0) {
                    printf("Error while writing: %d\n", errno);
                    
                    // The best way to break all cycles.
                    goto end; 
                }
                
                bytes_write += count;
            
            // Sometimes we cannot write all bytes per call.
            } while (bytes_write < bytes_read);
        } 
    }
    
    end:
    
    for (uint32_t i = 0; i < (argc - is_append + 1); ++i) {
        close(files[i]);
    }
    
    free(files);
    free(buffer);
    
    if (bytes_read < 0) {
        printf("Error while reading: %d\n", errno);
    }
    
    return errno;
}