#include <stdio.h>

// Prints `symbol` `count` times.
void print_char(char symbol, int count) {
	for (int i = 0; i < count; ++i) {
		printf("%c", symbol);
	}
}

// Prints triangle of height `size` and width 2 * `size` - 1.
void print_triangle(int size) {
	for (int i = 0; i < size; ++i) {
		print_char(' ', size - i - 1);
		print_char('*', i * 2 + 1);
		
		printf("\n");
	}	
}

int main(int argc, char* argv[]) {
	if (argc != 2) {
		return -1;
	}
	
	int size;
	
	sscanf(argv[1], "%d", &size);
	
	print_triangle(size);
	
	return 0;
}