#include <stdio.h>

// Swap two ints.
void swap(int* left, int* right) {
	int tmp = *left;
	
	*left = *right;
	
	*right = tmp;
}

int main(int argc, char* argv[]) {
	int left, right;
	
	printf("Type two integers separeted by space: ");
	
	scanf("%d %d", &left, &right);
	
	swap(&left, &right);
	
	printf("%d %d\n", left, right);
	
	return 0;
}