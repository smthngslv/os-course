#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE 256

// Swap two chars.
void swap(char* left, char* right) {
	char tmp = *left;
	
	*left = *right;
	
	*right = tmp;
}

// Reverse order of the `string`.
void reverse_string(char string[]) {
	// -1 cause of NULL byte in the end, we should swap it.
	int length = strlen(string) - 1;
	
	// Go from start to the middle.
	for (int i = 0; i < length / 2; ++i) {
		swap(&string[i], &string[length - i - 1]);
	}
}

int main(int argc, char* argv[]) {
	char string[BUFFER_SIZE];
	
	fgets(string, BUFFER_SIZE, stdin);
	
	reverse_string(string);
	
	printf("%s", string);

	return 0;
}