#include <stdio.h>

// Prints `symbol` `count` times.
void print_char(char symbol, int count) {
	for (int i = 0; i < count; ++i) {
		printf("%c", symbol);
	}
}

// Prints triangle of height `size` and width 2 * `size` - 1.
void print_triangle(int size) {
	for (int i = 0; i < size; ++i) {
		print_char(' ', size - i - 1);
		print_char('*', i * 2 + 1);
		
		printf("\n");
	}	
}

// Prints right triangle of height `size` and width `size`.
void print_right_triangle(int size) {
	for (int i = 1; i <= size; ++i) {
		print_char('*', i);
		
		printf("\n");
	}	
}

// Prints obtuse triangle of height `size` and width `size` / 2 + 1.
void print_obtuse_triangle(int size) {
	for (int i = 1; i <= size; ++i) {
		if (i <= size / 2) {
			print_char('*', i);
		}
		else {
			print_char('*', size - i + 1);
		}
		
		
		printf("\n");
	}
}

// Prints rectangle of height `size` and width `size`.
void print_rectangle(int size) {
	for (int i = 1; i <= size; ++i) {
		print_char('*', size);
		
		printf("\n");
	}	
}

int main(int argc, char* argv[]) {
	if (argc != 3) {
		printf("Type type and size separated by space.\n");
		
		printf("0 - Triangle.\n");
		printf("1 - Right triangle.\n");
		printf("2 - Obtuse triangle.\n");
		printf("3 - Rectangle.\n");
	
		return -1;
	}
	
	int type, size;
	
	sscanf(argv[1], "%d", &type);
	sscanf(argv[2], "%d", &size);
	
	switch(type) {
		case 0:
			print_triangle(size);
			break;
			
		case 1:
			print_right_triangle(size);
			break;
			
		case 2:
			print_obtuse_triangle(size);
			break;
			
		case 3:
			print_rectangle(size);
			break;
			
		default:
			printf("Wront type: %d\n", type);
			break;
	}
	
	return 0;
}