#include <stdio.h>
#include <float.h>   // For FLT_MAX and DBL_MAX
#include <limits.h>  // For INT_MAX


// Ivan Izmailov


int main() {
	// Declaration.
	int integerValue;
	float floatValue;
	double doubleValue;
	
	// Assigning
	integerValue = INT_MAX;
	floatValue   = FLT_MAX;
	doubleValue  = DBL_MAX;
	
	// Output
	printf("Integer -> size: %ld bytes | value: %d\n", sizeof(integerValue), integerValue);
	printf("Float   -> size: %ld bytes | value: %f\n", sizeof(floatValue),  floatValue);
	printf("Double  -> size: %ld bytes | value: %f\n", sizeof(doubleValue), doubleValue);
	
	return 0;
}