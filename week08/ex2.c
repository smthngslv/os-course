#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#define TIME_SECONDS           10
#define MEMORY_TO_ALLOCATE_MB  1024

/* Comment:

When I start the program and run `vmstat 1`,
I can see, that every second size of free memory decrease 
by apx. 1 millon bytes (1 GB). When the program stops, 
the memory takes back the initial values. The `si` and `so` 
are always zero, even when I allocate 10GB. 

*/

int32_t main(void) {
    uint8_t* buffers[TIME_SECONDS];
    
    const uint32_t size = MEMORY_TO_ALLOCATE_MB * 1024 * 1024;
    
    for (uint32_t i = 0; i < TIME_SECONDS; ++i) {
        buffers[i] = (uint8_t*)malloc(size);
        
        memset(buffers[i], 0, size);
        
        sleep(1);
    }
    
    for (uint32_t i = 0; i < TIME_SECONDS; ++i) {
        free(buffers[i]);
    }
    
    return EXIT_SUCCESS;
}
