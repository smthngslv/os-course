#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <sys/resource.h>

#define TIME_SECONDS           10
#define MEMORY_TO_ALLOCATE_MB  1024

int32_t main(void) {
    uint8_t* buffers[TIME_SECONDS];
    
    const uint32_t size = MEMORY_TO_ALLOCATE_MB * 1024 * 1024;
    
    struct rusage usage;
    
    for (uint32_t i = 0; i < TIME_SECONDS; ++i) {
        buffers[i] = (uint8_t*)malloc(size);
        
        memset(buffers[i], 0, size);
        
        if (getrusage(RUSAGE_SELF, &usage)) {
            printf("Cannot get usage.");
            
            break;
        }
        
        printf("Memory used: %ld KB\n", usage.ru_maxrss);
        
        sleep(1);
    }
    
    for (uint32_t i = 0; i < TIME_SECONDS; ++i) {
        free(buffers[i]);
    }
    
    return EXIT_SUCCESS;
}
