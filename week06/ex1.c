#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


#define FILE_NAME "input.txt"

#define                      SUCCESS 0
#define       CANNOT_OPEN_FILE_ERROR 1
#define            EOF_REACHED_ERROR 2
#define CANNOT_ALLOCATE_MEMORY_ERROR 3


struct Process {
    uint32_t id;
    
    uint32_t arrival_time;
    uint32_t burst_time;
    
    uint32_t exit_time;
};

typedef struct Process Process_t;

void fcfs_sheduler(const uint32_t processes_count, Process_t* const processes) {
    uint32_t current_tick = 0;
    
    for (uint32_t i = 0; i < processes_count; ++i) {
        // If no processes in the time (CPU is waiting).
        if (processes[i].arrival_time > current_tick) {
            // Move forward in time, when a new process has come.
            current_tick = processes[i].arrival_time + processes[i].burst_time;
        }
        else {
            // Otherwise exucute current process.
            current_tick += processes[i].burst_time;
        }
        
        processes[i].exit_time = current_tick;
    }
}

// Prints the output.
void out(const uint32_t processes_count, Process_t* const processes) {    
    uint32_t         completion_time = 0;
    uint32_t    average_waiting_time = 0;
    uint32_t average_turnaround_time = 0;
    
    printf("P#\tAT\tBT\tET\tTAT\tWT\n\n");
    
    for (uint32_t i = 0; i < processes_count; ++i) {
        uint32_t turnaround_time = processes[i].exit_time - processes[i].arrival_time;
        uint32_t waiting_time    = turnaround_time - processes[i].burst_time;
        
        average_waiting_time    += waiting_time;
        average_turnaround_time += turnaround_time;
        
        if (processes[i].exit_time > completion_time) {
            completion_time = processes[i].exit_time;
        }
        
        printf("P%d\t%d\t%d\t%d\t%d\t%d\n", processes[i].id, 
                                            processes[i].arrival_time, 
                                            processes[i].burst_time,
                                            processes[i].exit_time,
                                            turnaround_time,
                                            waiting_time);
    }
    
    printf("\nCompletion Time:\t\t%d\n", completion_time);
    printf("Average Waiting Time:\t\t%f\n",  (float)average_waiting_time    / processes_count);
    printf("Average Turnaround Time:\t%f\n", (float)average_turnaround_time / processes_count);
}

/*
Read input file.
Format:
4       - Processes count.
1 2 3 4 - Processes arrival times.
2 4 1 5 - Processes burst times.

Returns:
0 - Success.
1 - CANNOT_OPEN_FILE_ERROR
2 - EOF_REACHED_ERROR
3 - CANNOT_ALLOCATE_MEMORY
*/
int32_t read_input_file(char* const file_name, uint32_t* const processes_count, Process_t** const processes) {
    FILE* file = fopen(file_name, "r");
    
    if (!file) {
        return CANNOT_OPEN_FILE_ERROR;
    }
    
    if (!fscanf(file, "%d", processes_count)) {
        fclose(file);
        
        return EOF_REACHED_ERROR;
    }
    
    
    *processes = (Process_t*)malloc(sizeof(Process_t) * *processes_count);
    
    if (!processes) {
        return CANNOT_ALLOCATE_MEMORY_ERROR;
    }
    
    for (uint32_t i = 0; i < *processes_count; ++i) {
        if (!fscanf(file, "%d", &(*processes)[i].arrival_time)) {
            free(*processes);
            
            fclose(file);
            
            return EOF_REACHED_ERROR;
        }
        
        (*processes)[i].id = i + 1;
        (*processes)[i].exit_time = 0;
    }        
    
    for (uint32_t i = 0; i < *processes_count; ++i) {
        if (!fscanf(file, "%d", &(*processes)[i].burst_time)) {
            free(*processes);
            
            fclose(file);
            
            return EOF_REACHED_ERROR;
        }
    }
    
    fclose(file);
    
    return SUCCESS;
}

// Swap two processes.
void swap(Process_t* left, Process_t* right) {
	Process_t tmp = *left;
	
	*left = *right;
	
	*right = tmp;
}

// Sort by Arrival Time.
void sort_input(const uint32_t processes_count, Process_t* const processes) {                
    for (uint32_t i = 0; i < processes_count; ++i) {
        for (uint32_t j = i; j < processes_count; ++j) {
            if (processes[i].arrival_time > processes[j].arrival_time) {
                swap(&processes[i], &processes[j]);
            }
        }
    }
}


int32_t main(void) {
    int32_t result;
    
    uint32_t processes_count;
    Process_t* processes;
    
    if (result = read_input_file(FILE_NAME, &processes_count, &processes)) {
        printf("Cannot read file: %d\n", result);
        
        return result;
    }
    
    sort_input(processes_count, processes);
    
    fcfs_sheduler(processes_count, processes);
    
    out(processes_count, processes);
    
    return 0;
}