#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>


#define  INPUT_FILE_NAME "ex1.txt"
#define OUTPUT_FILE_NAME "ex1.memcpy.txt"
#define  STRING_TO_WRITE "This is a nice day!"


int32_t main(void) {
    int32_t input_file = open(INPUT_FILE_NAME, O_RDONLY);
    
    if (input_file < 0) {
        printf("Cannot open the input file.\n"); 
        
        return 1;
    }
    
    
    int32_t output_file = open(OUTPUT_FILE_NAME, O_RDWR | O_CREAT, S_IWRITE | S_IREAD);
    
    if (output_file < 0) {
        printf("Cannot open the output file.\n"); 
        
        close(input_file);
        
        return 2;
    }
    
    
    struct stat info;
    
    if (stat(INPUT_FILE_NAME, &info)) {
        printf("Cannot get the input file info.\n");
        
        close(input_file);
        close(output_file);
        
        return 3;
    }
    
    
    // Resize the file.
    ftruncate(output_file, info.st_size);
    
    
    char*  input_file_mapped = (char*)mmap(0, info.st_size,  PROT_READ, MAP_SHARED,  input_file, 0);
    
    if (input_file_mapped == MAP_FAILED) {
        printf("Cannot map the input file.\n");
        
        close(input_file);
        close(output_file);
        
        return 4;
    }
    
    
    char* output_file_mapped = (char*)mmap(0, info.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, output_file, 0);
    
    if (output_file_mapped == MAP_FAILED) {
        printf("Cannot map the output file.\n");
        
        close(input_file);
        close(output_file);
        
        return 5;
    }
    
    memcpy(output_file_mapped, input_file_mapped, info.st_size);
    
    munmap(input_file_mapped, info.st_size);
    munmap(output_file_mapped, info.st_size);
    
    close(input_file);
    close(output_file);
    
    return 0;
}