#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>


#define FILE_NAME       "ex1.txt"
#define STRING_TO_WRITE "This is a nice day!"


int32_t main(void) {
    int32_t file = open(FILE_NAME, O_RDWR);
    
    if (file < 0) {
        printf("Cannot open the file.\n"); 
        
        return 1;
    }
    
    
    struct stat info;
    
    if (stat(FILE_NAME, &info)) {
        printf("Cannot get the file info.\n");
        
        close(file);
        
        return 2;
    }
    
    
    // Multipling in case unicode.
    uint32_t size = strlen(STRING_TO_WRITE) * sizeof(char);
    
    // Resize the file.
    ftruncate(file, size);
    
    
    char* file_mapped = (char*)mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, file, 0);
    
    if (file_mapped == MAP_FAILED) {
        printf("Cannot map the file.\n");
        
        close(file);
        
        return 3;
    }
    
    memcpy(file_mapped, STRING_TO_WRITE, size);
    
    munmap(file_mapped, size);
    
    close(file);
    
    return 0;
}