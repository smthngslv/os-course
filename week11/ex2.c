#include <stdio.h>
#include <stdint.h>
#include <unistd.h>


#define BUFFER_SIZE 128


int32_t main(void) {
    char buffer[BUFFER_SIZE];
    
    if (setvbuf(stdout, buffer, _IOLBF, BUFFER_SIZE)) {
        printf("Cannot set the buffer.\n");
        
        return 1;
    }
    
    // Cycles for untrue coders. c:
    printf("H"); sleep (1);
    printf("e"); sleep (1);
    printf("l"); sleep (1);
    printf("l"); sleep (1);
    printf("o"); sleep (1);
    
    // For nice output.
    printf("\n");
    
    return 0;
}



