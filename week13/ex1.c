#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>


#define INPUT_FILE_NAME "input.txt"


void read_vector(FILE* file, uint32_t* const vector, const uint32_t amount) {
    for (uint32_t i = 0; i < amount; ++i) {
        fscanf(file, "%u", &vector[i]);
    }
}

int32_t main(const int32_t argc, const char** argv) {
    if (argc != 3) {
        printf("Using: ./ex1 PROCESSES RESOURCES"); return 1;
    }
    
    uint32_t processes;
    uint32_t resources;
    
    if (!sscanf(argv[1], "%u", &processes) || !sscanf(argv[2], "%u", &resources)) {
        printf("Using: ./ex1 PROCESSES RESOURCES"); return 1;
    }

    FILE* input = fopen(INPUT_FILE_NAME, "r");
    
    if (!input) {
        printf("Cannot open: %s\n", INPUT_FILE_NAME); return 2;
    }

    uint32_t* existence = (uint32_t*)malloc(sizeof(uint32_t) * resources);
    uint32_t* available = (uint32_t*)malloc(sizeof(uint32_t) * resources);
    
    uint32_t** current = (uint32_t**)malloc(sizeof(uint32_t*) * processes);
    uint32_t** request = (uint32_t**)malloc(sizeof(uint32_t*) * processes);
    
    read_vector(input, existence, resources);
    read_vector(input, available, resources);
    
    for (uint32_t i = 0; i < processes; ++i) {
        current[i] = (uint32_t*)malloc(sizeof(uint32_t) * resources);
        
        read_vector(input, current[i], resources);
    }        
        
    for (uint32_t i = 0; i < processes; ++i) {
        request[i] = (uint32_t*)malloc(sizeof(uint32_t) * resources);
        
        read_vector(input, request[i], resources);
    }    
       
    
    uint32_t current_process = 0;
       
    do {
        uint32_t     is_finish = 1;
        uint32_t can_be_finish = 1;
        
        for (uint32_t i = 0; i < processes; ++i) {
            if (request[current_process][i] || current[current_process][i]) {
                is_finish = 0;
                
                if (request[current_process][i] > available[i]) {
                    can_be_finish = 0; break;
                }
            }
        }
        
        if (is_finish == 0 && can_be_finish) {
            for (uint32_t i = 0; i < processes; ++i) {
                available[i] += request[current_process][i] + current[current_process][i];
                
                request[current_process][i] = 0;
                current[current_process][i] = 0;
            }
            
            // To go again througth all processes.
            current_process = 0;
        }
    
    // If do not reset the counter, it means that we cannot finish any process or we finish all of them.
    } while (++current_process < processes);
    

    uint32_t deadlocks = 0;

    for (uint32_t i = 0; i < processes; ++i) {
        for (uint32_t j = 0; j < resources; ++j) {
            if (request[i][j]) {
                ++deadlocks; break;
            }
        }
    }
    
    if (deadlocks) {
        printf("Deadlocks: %u\n", deadlocks);
    }
    else {
        printf("No deadlocks.\n");
    }
  
    free(existence);
    free(available);
    
    for (uint32_t i = 0; i < processes; ++i) {
        free(current[i]);
        free(request[i]);
    }
    
    free(current);
    free(request);
    
    return 0;
}