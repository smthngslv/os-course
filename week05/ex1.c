#include <stdio.h>
#include <stdint.h>
#include <pthread.h>

#define THREADS 4u

// Thread handler.
void* thread(void* argv) {
	uint32_t id = *((uint32_t *)argv);
	
	printf("The thread %d is running.\n", id);
	printf("The thread %d is working.\n", id);
	printf("The thread %d is finished.\n", id);
	
	pthread_exit(NULL);
}

int32_t main(void) {
	/* First part. Unsynchronized.
	// We cannot predict the output, cause the threads are running in parallel.
	pthread_t threads_id[THREADS];
	
	// Start all threads.
	for (uint32_t id = 0; id < THREADS; ++id) {
		printf("Creating thread %d.\n", id);
		
		pthread_create(&threads_id[id], NULL, &thread, &id);
	} 
	
	// Join all threads.
	for (uint32_t id = 0; id < THREADS; ++id) {
		pthread_join(threads_id[id], NULL);
		
		printf("Thread %d is joined.\n", id);
	} 
	
	*/
	
	/* Second part. Synchronized. */
	// Allways in stricly order.
	pthread_t thread_id;

	for (uint32_t id = 0; id < THREADS; ++id) {
		printf("Creating thread %d.\n", id);
		
		// Start.
		pthread_create(&thread_id, NULL, &thread, &id);
		
		// Join.
		pthread_join(thread_id, NULL);
		
		printf("Thread %d is joined.\n", id);
	}
	
	return 0;
}