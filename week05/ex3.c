#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>


#define BUFFER_SIZE  64u
#define MAX_RANDOM  100u


uint32_t isReaded  = 0u;
uint32_t isWrited  = 0u;
uint32_t isWorking = 0u;


uint32_t writedTimes = 0u;
uint32_t readedTimes = 0u;


void* producer(void* argv) {
	char* buffer = (char*)argv;
	
	while (isWorking) {
		if (isReaded) {
			//write_into_buffer
			snprintf(buffer, BUFFER_SIZE, "Random number: %d", rand() % MAX_RANDOM);
			
			isWrited = 1u;
			isReaded = 0u;
			
			++writedTimes;
		}
	}
	
	pthread_exit(NULL);
}


void* consumer(void* argv) {
	char* buffer = (char*)argv;
	
	while (isWorking) {
		if (isWrited) {
			//read_from_buffer
			//printf("%s\n", buffer);
			
			isWrited = 0u;
			isReaded = 1u;
			
			++readedTimes;
		}
	}
	
	pthread_exit(NULL);
}


int32_t main(void) {
	srand(time(NULL));
	
	pthread_t producer_tid;
	pthread_t consumer_tid;
	
	isReaded  = 1u;
	isWrited  = 0u;
	isWorking = 1u;
	
	char* buffer = (char*)malloc(sizeof(char) * BUFFER_SIZE);
	
	pthread_create(&producer_tid, NULL, &producer, buffer);
	pthread_create(&consumer_tid, NULL, &consumer, buffer);
	
	
	uint32_t lastReadedTimes = 0;
	uint32_t lastWritedTimes = 0;
	
	// While number of reads or writes is increasing.
	// For me it take 55k iterations: R: 55819 W: 55819 
	while(lastReadedTimes != readedTimes || lastWritedTimes != writedTimes) {
		printf("R: %d W: %d\n", readedTimes, writedTimes);
	
		lastReadedTimes = readedTimes;
		lastWritedTimes = writedTimes;
	
		sleep(1u);
	}
	
	printf("System is freezed after R: %d W: %d\n", readedTimes, writedTimes);
	
	isWorking = 0;
	
	// Join.
	pthread_join(producer_tid, NULL);
	pthread_join(consumer_tid, NULL);
	
	free(buffer);
	
	return 0;
}