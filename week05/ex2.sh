
# Test, if the numbers.txt doesn't exist.
if test ! -f numbers.txt
then 
	# Create it.
	echo 0 > numbers.txt
fi

# If there is no .lock file, we create it and do work, otherwise do nothing.
if ln numbers.txt numbers.txt.lock
then
	# Get last number.
	last=`tail -n 1 numbers.txt`
	
	# Add 1 to the last number and write it into numbers.txt.
	expr $last + 1 >> numbers.txt

	# Unblock.
	rm numbers.txt.lock
fi
