#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// Amount of last cycles to be count. 1 -- 32.
#define CYCLE_COUNT 16

// Input file.
#define  FILE_NAME "Lab 09 input.txt"


typedef struct {
    uint32_t index;
    uint32_t access;
    
} page_t;


// Returns 0 if miss and 1 if hit.
uint32_t access_page(const uint32_t page, page_t* const cached_pages, const uint32_t cache_size) {
    // Stores the index of the hit, if no hit store cache_size;
    uint32_t hit_index = cache_size;
    
    // Stores the index of the most infrequently used page.
    uint32_t min_access_index = 0;
    
    for (uint32_t i = 0; i < cache_size; ++i) {
        // Try to find the page in cache.
        if (cached_pages[i].index == page) {
            hit_index = i;
        }
        
        // Simultaneously find the most infrequently used page or
        // free space in the buffer, in case if cannot find the page in cache.
        if (cached_pages[min_access_index].access > cached_pages[i].access || cached_pages[i].index == 0) {
            // Now should make a right shift, because do not need accuracy anymore, found a new min_access_index.
            cached_pages[min_access_index].access >>= 1;
            
            min_access_index = i;
        }
        // Make right shift only in case it is not a min_access_index, because
        // need to save accuracy. If make a right shift - lost the data.
        else {
            // Right shift.
            cached_pages[i].access >>= 1;
        }
    }
    
    // If miss.
    if (hit_index == cache_size) {   
        // Cache the page.
        cached_pages[min_access_index].index = page;
        // Reset the counter.
        cached_pages[min_access_index].access = (1 << (CYCLE_COUNT - 1));
        
        return 0;
    }
    
    // If hit, set the MSB to 1.
    cached_pages[hit_index].access |= (1 << (CYCLE_COUNT - 1));
    
    return 1;
}


int32_t main(int32_t argc, char* argv[]) { 
    if (argc != 2) {
        printf("You should provide the number of page frames.\n");
        
		return 1;
	}
	
	uint32_t cache_size;
	
	if (!sscanf(argv[1], "%d", &cache_size)) {
        printf("Cannot parse: %s\n", argv[1]);
        
        return 2;
    }
	
    FILE* input = fopen(FILE_NAME, "r");
    
    if (!input) {
        printf("Cannot open file.\n"); 
        
        return 3;
    }
    
    // Init to zeros.
    page_t* cached_pages = calloc(cache_size, sizeof(page_t));
    
    // Current accesing page.
    uint32_t page_index;
    
    // Count of hits.
    uint32_t hits = 0;
    
    // Count of misses.
    uint32_t misses = 0;
    
    while (fscanf(input, "%d", &page_index) != EOF) {
        if (access_page(page_index, cached_pages, cache_size)) {
            ++hits; continue;
        }
        
        ++misses;
    }
    
    fclose(input);
    free(cached_pages);
    
    printf("HITS: %d\tMISS: %d\tTOTAL: %d\tRATIO: %f\n", hits, misses, hits + misses, (float)hits / misses);
    
    return 0;
}