# Compile.
gcc ex1.c -o ex1

for n in `seq 1 10`
do
	./ex1 $n
done

# Remove.
rm ex1

echo
# Prints explanations.
cat ex1.txt

echo
