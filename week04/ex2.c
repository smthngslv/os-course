#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>

#include <unistd.h>


int32_t main(void) {
	for (uint32_t i = 0; i < 5; ++i) {
		fork();
	}
	
	sleep(5);
	
	return 0;
}