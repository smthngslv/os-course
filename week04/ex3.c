#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <sys/wait.h>
#include <sys/types.h>

#include <unistd.h>


#define INPUT_BUFFER_SIZE 128 // Maximum of the input.


/***
 *     _   __ _  _    _                                                       
 *    | | / /(_)| |  | |                                                      
 *    | |/ /  _ | |_ | |_  _   _                                              
 *    |    \ | || __|| __|| | | |                                             
 *    | |\  \| || |_ | |_ | |_| |                                             
 *    \_| \_/|_| \__| \__| \__, |                                             
 *                          __/ |                                             
 *                         |___/                                              
 *     _____                            _                      _____    _____ 
 *    /  __ \                          | |                    |  _  |  |  _  |
 *    | /  \/  ___   _ __   ___   ___  | |  ___        __   __| |/' |  | |_| |
 *    | |     / _ \ | '_ \ / __| / _ \ | | / _ \       \ \ / /|  /| |  \____ |
 *    | \__/\| (_) || | | |\__ \| (_) || ||  __/        \ V / \ |_/ /_ .___/ /
 *     \____/ \___/ |_| |_||___/ \___/ |_| \___|         \_/   \___/(_)\____/ 
 *                                                                            
 *                                                                            
 */


int32_t main(void) {
	char* buffer   = (char*)malloc(sizeof(char) * INPUT_BUFFER_SIZE);
	char* command  = (char*)malloc(sizeof(char) * INPUT_BUFFER_SIZE);
	char* template = (char*)malloc(sizeof(char) * 32);
	
	char* argv[2];
	
	// Generate the template to safely read.
	snprintf(template, 32, "%%%d[^\n]%%*c", INPUT_BUFFER_SIZE - 1);
	
	printf("\n~$ ");
	
	while (scanf(template, buffer) == 1) {		
		argv[0] = buffer;
		argv[1] = NULL;
		
		// Generate command.
		snprintf(command, INPUT_BUFFER_SIZE, "/bin/%s", argv[0]);

		pid_t pid = fork();
		
		// Child process.
		if (pid == 0){
			return execve(command, argv, NULL);
		}
		
		int32_t status;
		
		// Wait.
		while(waitpid(pid, &status, WNOHANG) != pid) {}
		
		if (status) {
			printf("Error: %d\n", status);
		}
		
		printf("\n~$ ");
	}
	
	free(buffer);
	free(command);
	free(template);
	
	return 0;
}