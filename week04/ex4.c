#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <sys/wait.h>
#include <sys/types.h>

#include <unistd.h>


#define MAX_BACKGROUND_PROCESSES  2  // Maximum count of processes running in background.
#define INPUT_BUFFER_SIZE 		128  // Maximum of the input.
#define MAX_ARGC 		          8	 // Maximum count of argumets passing to a program.


/***
 *     _   __ _  _    _                                                     
 *    | | / /(_)| |  | |                                                    
 *    | |/ /  _ | |_ | |_  _   _                                            
 *    |    \ | || __|| __|| | | |                                           
 *    | |\  \| || |_ | |_ | |_| |                                           
 *    \_| \_/|_| \__| \__| \__, |                                           
 *                          __/ |                                           
 *                         |___/                                            
 *     _____                            _                      __     _____ 
 *    /  __ \                          | |                    /  |   |  _  |
 *    | /  \/  ___   _ __   ___   ___  | |  ___        __   __`| |   | |/' |
 *    | |     / _ \ | '_ \ / __| / _ \ | | / _ \       \ \ / / | |   |  /| |
 *    | \__/\| (_) || | | |\__ \| (_) || ||  __/        \ V / _| |_ _\ |_/ /
 *     \____/ \___/ |_| |_||___/ \___/ |_| \___|         \_/  \___/(_)\___/ 
 *                                                                                                                                                  
 */


// Split `line` into lines by `splitter`. Modifies `line` and `out`.
//
// `line` 	   - the line to be splitted.
// `splitter`  - the char by which the string is divided.
// `out`	   - the array of pointer to the splitted lines. There will be the result.
// `max_split` - the maximum count of parts. Basically the lenght of `out`.
//
// Returns the number of splitted lines.
uint32_t split_line(char* const line, const char splitter, char** const out, const uint32_t max_split) {
	uint32_t position_begin = 0u;
	uint32_t position_end   = 0u;
	
	// Amout of the partition.
	uint32_t splitted = 0u;
	
	// Until the end of the string or exceed maximum of splittions.
	while (line[position_end] && splitted < max_split) {		
		if (line[position_end++] == splitter) {
			// If there are several splitters in a row, we just skip them.
			while (line[position_end] == splitter) { ++position_end; }
			
			// If we reach the end of the string.
			if (!line[position_end]) {
				// We will split in the end. 
				break;
			}
			
			// Save the pointer to the begin of the new line.
			out[splitted++] = line + position_begin;
			
			// The line should ends by NULL.
			line[position_end - 1u] = 0;
			
			// Move forward.
			position_begin = position_end;
		}
	}
	
	// If we do not exceed maximum of splittions, so we reached the end of the line.
	if (!line[position_end]) {
		// Save the pointer to the begin of the new line. Write last part.
		out[splitted++] = line + position_begin;
	}
	// Otherwise we should recover last changed splitter to do not lose end of the line.
	else {
		line[position_end - 1u] = splitter;
	}
	
	return splitted;
}


int32_t main(void) {
	char* buffer   = (char*)malloc(sizeof(char) * INPUT_BUFFER_SIZE);
	char* command  = (char*)malloc(sizeof(char) * INPUT_BUFFER_SIZE);
	char* template = (char*)malloc(sizeof(char) * 32);
	
	char** argv = (char**)malloc(sizeof(char*) * MAX_ARGC);
	
	// To store background processes.
	uint32_t background_count = 0;
	pid_t backgrounds[MAX_BACKGROUND_PROCESSES] = {0};
	
	// Generate the template to safely read.
	snprintf(template, 32, "%%%d[^\n]%%*c", INPUT_BUFFER_SIZE - 1);
	
	// Print help.
	printf("\nUse `&` in the end of the command to run in background and type `exit` to exit. \n\n\n");
	
	while (1) {		
		// Check status of background processes.
		// If we do not do this, all background processes will be wait with status zombie.
		for (uint32_t i = 0; i < MAX_BACKGROUND_PROCESSES; ++i) {
			int32_t status;
			
			if (backgrounds[i] && waitpid(backgrounds[i], &status, WNOHANG) == backgrounds[i]) {
				printf("Process with PID: %d finished with status: %d\n\n", backgrounds[i], status);
				
				backgrounds[i] = 0;
				
				--background_count;
			}
		}
	
		printf("~$ ");
	
		// Empty line.
		if (scanf(template, buffer) != 1) {
			// Read \n charaster.
			getchar();
			
			continue;
		}
	
		// The command `exit`;
		if (strncmp(buffer, "exit", 5) == 0) {
			break;
		}
	
	
		uint32_t argc = split_line(buffer, ' ', argv, MAX_ARGC);
		
		// Generate command.
		snprintf(command, INPUT_BUFFER_SIZE, "/bin/%s", argv[0]);
		
		
		uint32_t isBackground = 0;
		
		// If the input ends by `&` then it is a background process.
		if (argv[argc - 1][0] == '&') {
			// Remove `&` from argv.
			argv[argc - 1] = NULL;
			
			// We can create one more background task.
			if (background_count < MAX_BACKGROUND_PROCESSES) {
				isBackground = 1;
			}
			else {
				printf("\nWARNING:\nThe command will be run in the current process, \nbecause maximum of background processes exceed.\n\n");
			}
		}
		
			
		pid_t pid = fork();
		
		// Child process.
		if (pid == 0){
			return execve(command, argv, NULL);
		}
		
		
		if (isBackground) {
			printf("Running in background, PID: %d\n\n", pid);
			
			uint32_t position = 0;
			
			// Finding free space.
			while(backgrounds[position++]) {
				if (position >= MAX_BACKGROUND_PROCESSES) {
					perror("Maximum of background processes exceed.\n");
					
					// Avoid overflow.
					--position;
					
					break;
				}
			}
			
			// Save pid.
			backgrounds[position - 1] = pid;
			
			++background_count;
		}
		else {
			int32_t status;
			
			// Wait.
			while(waitpid(pid, &status, WNOHANG) != pid) {}
			
			if (status) {
				printf("Error: %d\n\n", status);
			}
			else {
				printf("\n");
			}
		}
		
		// Clearing argv.
		for (int i = 0; i < MAX_ARGC; ++i) {
			argv[i] = NULL;
		}
	}
	
	free(buffer);
	free(command);
	free(template);
	free(argv);
	
	// TODO: Kill all child processes.
	
	return 0;
}