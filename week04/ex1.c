#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>

#include <unistd.h>


int32_t main(uint32_t argc, char *argv[]) {
	if (argc != 2u) {
		printf("There should be only one argument - number of the current attempt.\n");
		
		return -1;
	}

	int32_t n;
	
	// Read the N.
	sscanf(argv[1], "%d", &n);
	
	// Make child process. 
	pid_t isParent = fork();
	
	// If isParent != 0, then we are in the parent.
	if (isParent) {
		printf("Hello from parent [%d - %d]\n", getpid(), n);
		
		return 0;
	}
	// If isParent == 0, then we are in the child.
	else {
		printf("Hello from child [%d - %d]\n", getpid(), n);
	}
	
	return 0;
}